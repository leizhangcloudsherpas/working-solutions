@isTest
    public class WS_CommunitiesDataControllerTest {
    
        static testMethod void dataCon() {
            WS_CommunitiesDataController ws_dataCon = new WS_CommunitiesDataController();
            system.Test.setCurrentPageReference(new PageReference('')); 
            
            ws_dataCon.getSearchResultJSON();
            
           
    }
    static testMethod void dataCon2() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Atest';
                prog.Client__c = acct.Id;
                prog.Program_ID__c = 'ASD';
                
            insert prog;
            
            
            WS_CommunitiesDataController ws_dataCon = new WS_CommunitiesDataController();
            system.Test.setCurrentPageReference(new PageReference('/apex/WS_CommunitiesData?s=prog.Id&id=prog.Id')); 
            
            ws_dataCon.getSearchResultJSON();
            ws_dataCon.getMyKPIsByProgramJSON();
            
           
    }
    static testMethod void testgetChartJSON() {
            
            Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
            Account acc1 = new Account (
            Name = 'newAcc1'
            );  
            insert acc1;
            Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1'
            );
            insert conCase;
            //Create user
    
            Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
    
            User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
            );
            insert newUser1;
            
            System.runAs(newUser1) {
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Atest';
                prog.Client__c = acct.Id;
                prog.Program_ID__c = 'ASD';
                prog.Avg_Handle_Time_Goal__c = 0;
                
            insert prog;
            
            
            Program_Contact__c progcon = new Program_Contact__c();
                progcon.Contact__c = newUser1.contactId;
                progcon.Mapping_ID__c = 'TEST23';
            insert progcon;
              
            Agent_KPI__c agntKPI = new Agent_KPI__c();
                agntKPI.Program__c = prog.Id;
                agntKPI.Program_Contact__c = progcon.Id;
                agntKPI.KPI_Record_ID__c = 'TEST123';
                agntKPI.Date__c = date.today() - 7;
                agntKPI.AU_DK_Handled_Count__c = 1;
                //agntKPI.Local_Date__c = date.valueOf(stringDate);

            insert agntKPI;
            
            
            system.debug('agntKPI' + agntKPI.Program__c);
            system.debug('agntKPI' + agntKPI.Program_Contact__c);
            system.debug('agntKPI' + agntKPI.KPI_Record_ID__c);
            system.debug('agntKPI' + agntKPI.Date__c);
        
            WS_CommunitiesDataController ws_dataCon = new WS_CommunitiesDataController();
            system.Test.setCurrentPageReference(new PageReference('Page.WS_CommunitiesData')); 
            System.currentPageReference().getParameters().put('pid',prog.Id);

            ws_dataCon.getChartJSON();
            ws_dataCon.dateLegend(date.today());
            }

    }
    
}