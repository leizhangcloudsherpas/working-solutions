public with sharing class ProgramContactGenController {

    public Contact ct {get; set;}
    public List<Program__c> progs {get; set;}
    public Set<String> selectedFields {get; set;}
    public Set<String> selectedFieldsBefore {get; set;}
    public Set<SelectOption> progOptions {get; set;}

    public ProgramContactGenController(ApexPages.StandardController stdController) {
        this.ct = (Contact)stdController.getRecord();
        String str = ct.Name;
        this.progs = [SELECT Id, Name, 
                          (SELECT Id, Contact__r.Id FROM Program_Contacts__r WHERE Contact__r.Id = :this.ct.Id) 
                      FROM Program__c 
                      WHERE Active_Project__c = true];
        getProgOptions();
        getSelections();
    }

    public void getProgOptions() {
        progOptions = new Set<SelectOption>();
        for (Program__c prog : progs) {
            progOptions.add(new SelectOption(prog.Id, prog.Name));
        }
    }

    public void getSelections() {
        selectedFields = new Set<String>();
        for (Program__c prog : progs) {
            for (Program_Contact__c progCt : prog.Program_Contacts__r) {
                if (progCt.Contact__r.Id == ct.Id) {
                    selectedFields.add(prog.Id);
                }
                break;
            }
        }
        selectedFieldsBefore = selectedFields.clone();
    }

    public PageReference submit() {
        
        selectedFields.removeAll(selectedFieldsBefore);
        
        
        List<Program__c> progsToAdd = [SELECT Id, Name FROM Program__c WHERE (Id IN :selectedFields)];
        
        List<Program_Contact__c> progCts = new List<Program_Contact__c>();

        for(Program__c progToAdd : progsToAdd) {
            
                Program_Contact__c progCt = new Program_Contact__c();
                progCt.Contact__c = ct.Id;
                progCt.Program__c = progToAdd.Id;
                progCt.Email__c = ct.Email;
                progCt.Mapping_ID__c = genMappingId();
                progCt.Name = progCt.Mapping_ID__c;
    
                progCts.add(progCt);
        }
        insert progCts;
        
        PageReference pageRef = new PageReference('/' + ct.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    private String genMappingId() {
        String mappingId = 'O-';
        List<String> chars = new List<String> {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        for(Integer i = 0; i < 10; i++) {
            mappingId += chars[(Integer)(Math.random() * 26)];
        }
        return mappingId;
    }
    
    public void selectAll() {
        selectedFields = new Set<String>();
        for (Program__c prog : progs) {
            selectedFields.add(prog.Id);
        }
    }
}