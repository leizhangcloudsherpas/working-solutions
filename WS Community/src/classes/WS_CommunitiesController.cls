global with sharing class WS_CommunitiesController {

    global String username {get; set;}
    global String password {get; set;}
    global User me {get; set;}
    public List<String> pcids {get; set;}
    public Id progId {get;set;}
    public Program__c prog {get;set;}
    public list <Shared_Link__c>    shrdlinkList {get;set;}
    public boolean flag {get; set;}
    public list <Program_Contact__c>    progConList {get;set;}
    public list<Attachment> agentDocs {get; set;}
    public String nickname {get; set;}
    
    public void updateNickName() {
        me.CommunityNickname = nickname;
        update me;
    }
    public list<Program__c> getprogramLstAC() {
        return [Select Id, Name, Display_Name__c, Apply_URL__c From Program__c Where Major__c = TRUE AND (Name LIKE 'A%' OR NAME LIKE 'B%' OR NAME LIKE 'C%') ORDER BY Name ASC];
    }    
    public list<Program__c> getprogramLstDF() {
        return [Select Id, Name, Display_Name__c, Apply_URL__c From Program__c Where Major__c = TRUE AND (NAME LIKE 'D%' OR NAME LIKE 'E%' OR NAME LIKE 'F%') ORDER BY Name ASC];
    }    
    public list<Program__c> getprogramLstGM() {
        return [Select Id, Name, Display_Name__c, Apply_URL__c From Program__c Where Major__c = TRUE AND (NAME LIKE 'G%' OR Name LIKE 'H%' OR NAME LIKE 'I%' OR NAME LIKE 'J%' OR NAME LIKE 'K%' OR NAME LIKE 'L%' OR NAME LIKE 'M%') ORDER BY Name ASC];
    }
    
    public list<Program__c> getprogramLstNS() {
        return [Select Id, Name, Display_Name__c, Apply_URL__c From Program__c Where Major__c = TRUE AND (Name LIKE 'N%' OR NAME LIKE 'O%' OR NAME LIKE 'P%' OR NAME LIKE 'Q%' OR NAME LIKE 'R%' OR NAME LIKE 'S%') ORDER BY Name ASC];
    }
    
    public list<Program__c> getprogramLstTZ() {
        return [Select Id, Name, Display_Name__c, Apply_URL__c From Program__c Where Major__c = TRUE AND (Name LIKE 'T%' OR NAME LIKE 'U%' OR NAME LIKE 'V%' OR NAME LIKE 'W%' OR NAME LIKE 'X%' OR NAME LIKE 'Y%' OR NAME LIKE 'Z%') ORDER BY Name ASC];
    }
    public List<Program__c> getAssignedProgramList() {
        String[] ids = new String[]{};
        return [SELECT Id, Name, Chatter_Group_Id__c, Apply_URL__c, Avg_After_Call_Work_Goal__c, Avg_Handle_Time_Goal__c FROM Program__c WHERE Id IN (SELECT Program__c FROM Program_Contact__c WHERE Contact__c = :me.ContactId) ORDER BY NAME];
    }
    
    public Program__c getProgRec() {
        Id ProgId = ApexPages.currentPage().getParameters().get('id');
        return [Select Id, Name, My_Commute__c, Apply_URL__c, Program_Schedule__c, Display_Name__c, Support_Team__c, Chatter_Group_Id__c, Program_Overview__c, Requirements__c, Logo__c, Avg_After_Call_Work_Goal__c , Avg_Handle_Time_Goal__c , External_Chatter_Room__c From Program__c Where Id =: progId];
    }
    
    public list<Shared_Link__c> getshardlnkList() {
        
        return [Select Id, Name, URL__c, Video_Image__c, Type__c From Shared_Link__c Where Type__c = 'Video URL' AND Is_Active__c = TRUE];
    }
    
    public list<Shared_Link__c> getlatestNews() {
        
        return [Select Id, Name, URL__c, CreatedDate, Type__c From Shared_Link__c Where Type__c = 'Latest News' AND Is_Active__c = TRUE];
    }
    
    public list<Shared_Link__c> gettoolList() {
        
        return [Select Id, Name,  URL__c, Type__c From Shared_Link__c Where Type__c = 'Tools' AND Is_Active__c = TRUE];
    }
    
    public list<Shared_Link__c> getprepList() {
        
        return [Select Id, Name, Type__c, URL__c From Shared_Link__c Where Type__c = 'Preparation Resources' AND Is_Active__c = TRUE];
    }
    
    public WS_CommunitiesController(ApexPages.StandardController sc){
        this();
        populateAgentDocs();
        prog = (Program__c)sc.getRecord();
        progId = prog.Id;
        //prog = [Select Id, Name, Program_Overview__c, Requirements__c, Logo__c From Program__c];
        shrdlinkList = [Select Id, Name, Program__c, URL__c From Shared_Link__c Where Program__c =: prog.Id AND Type__c = 'Required Preparation on Program'];
        flag = false;
        progConList = [Select Id, Contact__r.Name, User_Image__c, Contact_Number__c, Email__c, Program__c From Program_Contact__c Where Program__c = :prog.Id];
        
    }
    
    public pagereference assigned() {
       /*flag = true;
       Agent_KPI__c assUser = new Agent_KPI__c();
       assUser.Program__c = prog.Id;
       assUser.User__c = userinfo.getuserId();
       insert assUser;*/
       return null;
    }
    
    global WS_CommunitiesController () {
        me = [SELECT Email, CommunityNickname , SmallPhotoUrl, FullPhotoUrl, ContactId from User where Id =: UserInfo.getUserId()][0];
        pcids = new List<String>();
        for (Program_Contact__c pc : [SELECT Id FROM Program_Contact__c WHERE Contact__c = :me.ContactId]) {
            pcids.add(pc.Id);
        }
    }
    
    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        if (String.isBlank(startUrl)) startUrl = '/';
        return Site.login(username, password, startUrl);
    }
    
    global PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/WS_CommunitiesLogin');//new PageReference('/login');
        }
        else{
            String startUrl = System.currentPageReference().getParameters().get('startURL');
            return (null == startUrl) ? null : new PageReference(startUrl);
        }
    }   
    global Integer getMyTotalCallsByProgram() {
        if (pcids.size() <= 0 ) return 0;
        Integer mc = 0;
        
        List<AggregateResult> aggre = [SELECT SUM(AU_DK_Handled_Count__c) myweeklytotal FROM Agent_KPI__c WHERE Program__c = :prog.Id AND Date__c = LAST_N_DAYS:7 AND Program_Contact__c IN :pcids];
        system.debug('@@@@@@@@agp' + aggre);
        
        for (AggregateResult a: [SELECT SUM(AU_DK_Handled_Count__c) myweeklytotal FROM Agent_KPI__c WHERE Program__c = :prog.Id AND Date__c = LAST_N_DAYS:7 AND Program_Contact__c IN :pcids]) {
            mc = (a.get('myweeklytotal') == null) ? 0 : ((Decimal)a.get('myweeklytotal')).intValue();
        
        }
        return mc;
        
    }
    global Integer getMyTotalCallDurationsByProgram() {// in minutes
        if (pcids.size() <= 0 ) return 0;
        Integer mc = 0;
        for (AggregateResult a: [SELECT SUM(AU_DK_Talk_Duration__c) myweeklytotal FROM Agent_KPI__c WHERE Program__c = :prog.Id AND Date__c = LAST_N_DAYS:7 AND Program_Contact__c IN :pcids]) {
            mc = (a.get('myweeklytotal') == null) ? 0 : ((Decimal)a.get('myweeklytotal')/60).intValue();
        }
        return mc;
    }
    global Integer getCompanyTotalCallsByProgram() {
        Decimal all = 0;
        Integer num = 0;
        /*for (AggregateResult a: [SELECT SUM(AU_DK_Talk_Duration__c) total FROM Agent_KPI__c WHERE Program__c = :prog.Id AND Date__c = LAST_N_DAYS:7 GROUP BY Agent_Email__c]) {
            all = all + (Decimal)a.get('total');
            num = num + 1;
        }**/
        return (num == 0) ? 0 : (1.0*all/num).intValue();
    }    
    global Integer getMyTotalCalls() {
        if (pcids.size() <= 0 ) return 0;
        Integer mc = 0;
        for (AggregateResult a: [SELECT SUM(AU_DK_Handled_Count__c) myweeklytotal FROM Agent_KPI__c WHERE Date__c = LAST_N_DAYS:7 AND Program_Contact__c IN :pcids]) {
            mc = (a.get('myweeklytotal') == null) ? 0 : ((Decimal)a.get('myweeklytotal')).intValue();
        }
        return mc;
    }
    global Integer getMyTotalCallDurations() {// in minutes
        if (pcids.size() <= 0 ) return 0;
        Integer mc = 0;
        for (AggregateResult a: [SELECT SUM(AU_DK_Talk_Duration__c) myweeklytotal FROM Agent_KPI__c WHERE Date__c = LAST_N_DAYS:7 AND Program_Contact__c IN :pcids]) {
            mc = (a.get('myweeklytotal') == null) ? 0 : ((Decimal)a.get('myweeklytotal')/60).intValue();
        }
        return mc;
    }    
    global Integer getCompanyTotalCalls() {
        Decimal all = 0;
        Integer num = 0;
        /** KPI FIX
        for (AggregateResult a: [SELECT SUM(AU_DK_Talk_Duration__c) total FROM Agent_KPI__c WHERE Date__c = LAST_N_DAYS:7 GROUP BY Agent_Email__c]) {
            all = all + (Decimal)a.get('total');
            num = num + 1;
        }
        **/
        return (num == 0) ? 0 : (1.0*all/num).intValue();
    }
    public void populateAgentDocs() {
            List<User> usrs = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            this.agentDocs = [SELECT Id, Name, ContentType, Description 
                              FROM Attachment 
                              WHERE ParentId in (SELECT Id 
                                                 FROM echosign_dev1__SIGN_Agreement__c 
                                                 WHERE echosign_dev1__Recipient__c = :usrs.get(0).ContactId 
                                                     AND echosign_dev1__StatusVisible__c = 'Signed')
                                  AND Name LIKE '%Signed%']; 
    }
}