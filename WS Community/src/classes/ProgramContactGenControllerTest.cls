@isTest
public with sharing class ProgramContactGenControllerTest {

    private static Contact makeContact() {
        Contact ct = new Contact();
        ct.LastName = 'Jones';
        insert ct;
        return ct;
    }
    
    private static List<Program__c> makePrograms() {
        Account client = new Account();
        client.Name = 'Client A';
        
        insert client;
        
        List<Program__c> progs = new List<Program__c>();
        
        for (Integer i = 1; i <=3; i++) {
            Program__c prog = new Program__c();
            prog.Client__c = client.Id;
            prog.Program_ID__c = 'progId'+i;
            prog.Name = 'Program ' + i;
            prog.Active_Project__c = true;
            progs.add(prog);
        }
        
        insert progs;
        
        return progs;
    }
    
    private static void makeProgramContact(Contact ct, Program__c prog) {
        Program_Contact__c progCt = new Program_Contact__c();
        progCt.Contact__c = ct.Id;
        progCt.Program__c = prog.Id;
        progCt.Mapping_ID__c = 'asdlfjasdlf';
        
        insert progCt;
    }
    
    @isTest 
    public static void existingProgramContacts() {
        Contact ct = makeContact();
		List<Program__c> progs = makePrograms();
        makeProgramContact(ct, progs.get(0));
        
        PageReference pageRef = Page.ProgramContactGen;
		Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ct);	
        ProgramContactGenController controller = new ProgramContactGenController(sc);
        
        System.assert(controller.selectedFields.size() == 1, 'Not one field selected initially.');
        
        Test.startTest();
        
        controller.selectedFields.add(progs.get(1).Id);
        pageRef = controller.submit();
        
        Test.stopTest();
        
        List<Program_Contact__c> progCts = [SELECT Id, Contact__c, Program__c FROM Program_Contact__c];
        
        System.assert(pageRef.getUrl() == new PageReference('/' + ct.Id).getUrl());
        System.assert(progCts.size() == 2, 'Incorrect number of Program Contacts. Size = ' + progCts.size());
        for (Program_Contact__c progCt : progCts) {
            System.assert(progCt.Contact__c == ct.Id, 'Program Contact assigned to wrong Contact.');
        }
        System.assert(progCts.get(0).Program__c != progCts.get(1).Program__c);
    }
    
    @isTest
    public static void noExistingProgramContacts() {
        Contact ct = makeContact();
		List<Program__c> progs = makePrograms();
        
        PageReference pageRef = Page.ProgramContactGen;
		Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ct);	
        ProgramContactGenController controller = new ProgramContactGenController(sc);
        
        Test.startTest();
        
        controller.selectedFields.add(progs.get(0).Id);
        controller.selectedFields.add(progs.get(1).Id);
        pageRef = controller.submit();
        
        Test.stopTest();
        
        List<Program_Contact__c> progCts = [SELECT Id, Contact__c, Program__c FROM Program_Contact__c];
        
        System.assert(pageRef.getUrl() == new PageReference('/' + ct.Id).getUrl());
        System.assert(progCts.size() == 2, 'Incorrect number of Program Contacts. Size = ' + progCts.size());
        for (Program_Contact__c progCt : progCts) {
            System.assert(progCt.Contact__c == ct.Id, 'Program Contact assigned to wrong Contact.');
        }
        System.assert(progCts.get(0).Program__c != progCts.get(1).Program__c);
    }
    
    @isTest
    public static void selectAll() {
        Contact ct = makeContact();
		List<Program__c> progs = makePrograms();
        
        PageReference pageRef = Page.ProgramContactGen;
		Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ct);	
        ProgramContactGenController controller = new ProgramContactGenController(sc);
        
        Test.startTest();
        
        controller.selectAll();
        pageRef = controller.submit();
        
        Test.stopTest();
        
        List<Program_Contact__c> progCts = [SELECT Id, Contact__c, Program__c FROM Program_Contact__c];
        
        System.assert(pageRef.getUrl() == new PageReference('/' + ct.Id).getUrl());
        System.assert(progCts.size() == 3, 'Incorrect number of Program Contacts. Size = ' + progCts.size());
        for (Program_Contact__c progCt : progCts) {
            System.assert(progCt.Contact__c == ct.Id, 'Program Contact assigned to wrong Contact.');
        }
        System.assert(progCts.get(0).Program__c != progCts.get(1).Program__c);
        System.assert(progCts.get(0).Program__c != progCts.get(2).Program__c);
        System.assert(progCts.get(1).Program__c != progCts.get(2).Program__c);
    }
    
}