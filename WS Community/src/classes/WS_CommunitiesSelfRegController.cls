/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public with sharing class WS_CommunitiesSelfRegController {

    public WS_ServiceCats.CatsPort CATS;
    public String CATSPassword { get; set; }
    public String CATSUserName { get; set; }
    public Integer CATSId { get; set; }
    public Integer step { get; set; }

    public PageReference checkCATSUser() {
        CATSId = CATS.authenticateUser(CATSUserName, CATSPassword); //'cloudsherpas@workingsol.com', 'Hy7DqcP82'
        if (CATSId <= 0) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid CATS User name and password.');
            ApexPages.addMessage(msg);
            return null;
        } else {
            //CATS.getCorporateName(CATSId);
            email = CATS.getEmailByIdCatsUser(CATSId);
            String name = CATS.getUserName(CATSId);
            String[] names = name.split(' ', 2);
            firstName = names[0].trim();
            lastName = (names.size() > 1) ? names[1].trim() : '';
            communityNickname = CATSUserName;
            //CATS.getUserProjectInvolvementList//my programs
            step = 2;
        }
        return null;
    }


    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    
    public WS_CommunitiesSelfRegController() {
        CATS = new WS_ServiceCats.CatsPort();
        step = 1;
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }

    public PageReference registerUser() {
    
           // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }    

        String profileId = null; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = ''; // To be filled in by customer.
        
        String userName = email;

        User u = new User();
        u.Agent_ID__c = String.valueOf(CATSId);
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.CommunityNickname = communityNickname;
        u.ProfileId = profileId;
        
        String userId = Site.createPortalUser(u, accountId, CATSPassword);//TODO
      
        if (userId != null) { 
            step = 3;
            /*if (password != null && password.length() > 1) {
                return Site.login(userName, password, ApexPages.currentPage().getParameters().get('startURL'));
            }
            else {
                step = 3;
            }*/
        }
        return null;
    }
}