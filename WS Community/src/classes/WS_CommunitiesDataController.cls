public with sharing class WS_CommunitiesDataController {
    public String CallBack {get; set;}
    public String RequestAction {get; set;}
    public List<String> pcids {get; set;}
    public User me {get; set;}
    public WS_CommunitiesDataController() {
          this.CallBack = (ApexPages.currentPage() != null) ? ApexPages.currentPage().getParameters().get('callback') : 'callback';
          if (String.isBlank(this.CallBack)) this.CallBack = 'callback';
          this.RequestAction = (ApexPages.currentPage() != null) ? ApexPages.currentPage().getParameters().get('action') : 'search';  
          if (String.isBlank(this.RequestAction)) RequestAction = 'search';
          me = [SELECT Email, CommunityNickname , SmallPhotoUrl, FullPhotoUrl, ContactId from User where Id =: UserInfo.getUserId()][0];
          pcids = new List<String>();
          for (Program_Contact__c pc : [SELECT Id FROM Program_Contact__c WHERE Contact__c = :me.ContactId]) {
              pcids.add(pc.Id);
          }
    }
    
    public String getSearchResultJSON() {
        try {
            String s = (ApexPages.currentPage() != null) ? ApexPages.currentPage().getParameters().get('s') : ''; 
            if (String.isBlank(s) || String.isBlank(RequestAction)) return '[]';
            s = '*' + s + '*';
            return JSON.serialize([FIND :s IN NAME FIELDS RETURNING Program__c(Id, Name), Shared_Link__c(Id, Name, URL__c) LIMIT 50]);
        } catch(Exception e) {
            System.debug(e);
            return '[]';
        }
    }

    public String getChartJSON() {

        String pid = (ApexPages.currentPage() != null) ? ApexPages.currentPage().getParameters().get('pid') : null;  
        if (String.isBlank(pid)) return '{}';
        List<Program__c> pc = [SELECT Id, Name, Avg_After_Call_Work_Goal__c, Avg_Handle_Time_Goal__c FROM Program__c WHERE Id = :pid];
        if (pc.size() <= 0) return '{}';
        Date t = Date.today();
        Map<String, Integer[]> per = new Map<String, Integer[]>();
        /*for (Integer i = 0; i < 7; i++) {// 7 days
            per.put(dateLegend(t-i), new Integer[]{0,0,0,0,0});
        }*/
        system.debug(pc);
        for (AggregateResult a: [SELECT WEEK_IN_YEAR(Local_Date__c) wk, SUM(AU_DK_Talk_Duration__c) totalAHT, SUM(AU_DK_Conventional_Acw__c) totalACW, SUM(AU_DK_Total_Productive_Duration__c) totalProductive
                                        FROM Agent_KPI__c 
                                        WHERE Program__c = :pid 
                                        AND Date__c = LAST_N_WEEKS:12
                                        AND Program_Contact__c IN :pcids
                                        GROUP BY WEEK_IN_YEAR(Local_Date__c)
                                        ORDER BY WEEK_IN_YEAR(Local_Date__c)]) {
            Decimal t1 = (Decimal)a.get('totalAHT')/3600;
            Integer totalAHT = (t1 == null) ? 0 : t1.intValue();
            Decimal t2= (Decimal)a.get('totalACW')/3600;
            Integer totalACW = (t2 == null) ? 0 : t2.intValue();
            Decimal t3= (Decimal)a.get('totalProductive')/3600;
            Integer totalProductive = (t3 == null) ? 0 : t3.intValue();
            system.debug(a);
            Integer aaht = (pc[0].Avg_Handle_Time_Goal__c == null) ? 0 : ((Decimal)pc[0].Avg_Handle_Time_Goal__c).intValue();
            Integer aacw = (pc[0].Avg_After_Call_Work_Goal__c == null) ? 0 : ((Decimal)pc[0].Avg_After_Call_Work_Goal__c).intValue();
            per.put(String.valueOf(a.get('wk')), new Integer[]{totalProductive, totalAHT, totalACW, aaht, aacw});
        }
        return JSON.serialize(per);
    }
    
    @TestVisible 
    private String dateLegend(Date d) {
        return (d == null) ? '' :  d.format();//dateLegend((Date)a.get('wk'))
    }
    
    public String getMyKPIsByProgramJSON() {
        String pid = (ApexPages.currentPage() != null) ? ApexPages.currentPage().getParameters().get('pid') : null;  
        if (String.isBlank(pid)) return '{}';
        Map<String, Integer> kpi = new Map<String, Integer>();
        for (AggregateResult a: [SELECT SUM(AU_DK_Talk_Duration__c) td, SUM(AU_DK_Total_Productive_Duration__c) pd 
                                 FROM Agent_KPI__c
                                 WHERE Program__c = :pid 
                                 AND Date__c = LAST_N_WEEKS:12 
                                 AND Program_Contact__c IN :pcids]) {
            kpi.put('td', ((Decimal)a.get('td')/3600).intValue());
            kpi.put('pd', ((Decimal)a.get('pd')/3600).intValue());
            
        }
        return JSON.serialize(kpi);
    }  
}