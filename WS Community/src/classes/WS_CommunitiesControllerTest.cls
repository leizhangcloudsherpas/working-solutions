@isTest
    public class WS_CommunitiesControllerTest {
    
        static testMethod void getshardlnkList() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Atest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Shared_Link__c shrdl = new Shared_Link__c();
                shrdl.Name = 'TestName';
                shrdl.Type__c = 'Video URL';
                shrdl.Is_Active__c = TRUE;
                shrdl.Program__c = prog.Id;
                
            insert shrdl;
            
            ApexPages.StandardController sc = new ApexPages.standardController(prog);
            WS_CommunitiesController ws_ComCon = new WS_CommunitiesController(sc);
            ws_ComCon.getshardlnkList();
            ws_ComCon.assigned();
            System.assertEquals(shrdl.Type__c, 'Video URL');
            ws_ComCon.getprogramLstAC();
            System.assertEquals(String.valueof(prog.Name).startsWith('A'),True);
            
        
        }
    
        static testMethod void getlatestNews() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Dtest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Shared_Link__c shrdl = new Shared_Link__c();
                shrdl.Name = 'TestName';
                shrdl.Type__c = 'Latest News';
                shrdl.Is_Active__c = TRUE;
                shrdl.Program__c = prog.Id;
                
            insert shrdl;
            
            ApexPages.StandardController sc = new ApexPages.standardController(prog);
            WS_CommunitiesController ws_ComCon = new WS_CommunitiesController(sc);
            ws_ComCon.getlatestNews();
            ws_ComCon.getlatestNews();
            System.assertEquals(shrdl.Type__c, 'Latest News');
            ws_ComCon.getprogramLstDF();
            System.assertEquals(String.valueof(prog.Name).startsWith('D'),True);
        
        }
        
        static testMethod void gettoolList() {
            
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Contact con = new Contact();
                con.LastName = 'TestCon';
                con.AccountId = acct.Id;
            insert con;
            
            Program__c prog = new Program__c();
                prog.Name = 'Gtest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Shared_Link__c shrdl = new Shared_Link__c();
                shrdl.Name = 'TestName';
                shrdl.Type__c = 'Tools';
                shrdl.Is_Active__c = TRUE;
                shrdl.Program__c = prog.Id;
                
            insert shrdl;
            
            ApexPages.StandardController sc = new ApexPages.standardController(prog);
            WS_CommunitiesController ws_ComCon = new WS_CommunitiesController(sc);
            ws_ComCon.gettoolList();
            ws_ComCon.login();
            System.assertEquals(shrdl.Type__c, 'Tools');
            ws_ComCon.getprogramLstGM();
            ws_ComCon.getCompanyTotalCalls();
            System.assertEquals(String.valueof(prog.Name).startsWith('G'),True);
        
        }
        
        static testMethod void getprepList() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Ntest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Shared_Link__c shrdl = new Shared_Link__c();
                shrdl.Name = 'TestName';
                shrdl.Type__c = 'Preparation Resources';
                shrdl.Is_Active__c = TRUE;
                shrdl.Program__c = prog.Id;
                
            insert shrdl;
            
            ApexPages.StandardController sc = new ApexPages.standardController(prog);
            WS_CommunitiesController ws_ComCon = new WS_CommunitiesController(sc);
            ws_ComCon.getprepList();
            ws_ComCon.getCompanyTotalCallsByProgram();
            ws_ComCon.getMyTotalCallsByProgram();
            System.assertEquals(shrdl.Type__c, 'Preparation Resources');
            ws_ComCon.getprogramLstNS();
            System.assertEquals(String.valueof(prog.Name).startsWith('N'),True);
        
        }
        
        static testMethod void getProgRec() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Ttest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Test.setCurrentPageReference(new PageReference('Page.WS_CommunitiesPrograms')); 
            System.currentPageReference().getParameters().put('Id',prog.Id);
            
            ApexPages.StandardController sc = new ApexPages.standardController(prog);
            WS_CommunitiesController ws_ComCon = new WS_CommunitiesController(sc);
            ws_ComCon.getProgRec();
            ws_ComCon.getAssignedProgramList();
            System.assertEquals(prog.Id, prog.Id);
            ws_ComCon.getprogramLstTZ();
            System.assertEquals(String.valueof(prog.Name).startsWith('T'),True);
        
        }
        
            static testMethod void TestgetMyTotalCallsByProgram() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Ttest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
            Account acc1 = new Account (
            Name = 'newAcc1'
            );  
            insert acc1;
            Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1'
            );
            insert conCase;
            //Create user
    
            Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
    
            User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
            //UserType = 'Guest'
            );
            insert newUser1;
            
            Program_Contact__c progcon = new Program_Contact__c();
                progcon.Contact__c = newUser1.contactId;
                progcon.Mapping_ID__c = 'TEST23';
            insert progcon;
                  
            Agent_KPI__c agntKPI = new Agent_KPI__c();
                agntKPI.Program__c = prog.Id;
                agntKPI.Program_Contact__c = progcon.Id;
                agntKPI.KPI_Record_ID__c = 'TEST123';
                agntKPI.Date__c = Date.today() - 4;
                agntKPI.AU_DK_Handled_Count__c = 1; 

            insert agntKPI;
            
            Agent_KPI__c agntKPI2 = new Agent_KPI__c();
                agntKPI2.Program__c = prog.Id;
                agntKPI2.Program_Contact__c = progcon.Id;
                agntKPI2.KPI_Record_ID__c = 'TEST124';
                agntKPI2.Date__c = Date.today() - 3;
                agntKPI2.AU_DK_Handled_Count__c = 1; 

            insert agntKPI2;
            
            Agent_KPI__c agntKPI3 = new Agent_KPI__c();
                agntKPI3.Program__c = prog.Id;
                agntKPI3.Program_Contact__c = progcon.Id;
                agntKPI3.KPI_Record_ID__c = 'TEST125';
                agntKPI3.Date__c = Date.today() - 3;
                agntKPI3.AU_DK_Handled_Count__c = 1; 

            insert agntKPI3;
            
            Agent_KPI__c agntKPI4 = new Agent_KPI__c();
                agntKPI4.Program__c = prog.Id;
                agntKPI4.Program_Contact__c = progcon.Id;
                agntKPI4.KPI_Record_ID__c = 'TEST126';
                agntKPI4.Date__c = Date.today() - 5;
                agntKPI4.AU_DK_Handled_Count__c = 1; 

            insert agntKPI4;
            
            Agent_KPI__c agntKPI5 = new Agent_KPI__c();
                agntKPI5.Program__c = prog.Id;
                agntKPI5.Program_Contact__c = progcon.Id;
                agntKPI5.KPI_Record_ID__c = 'TEST127';
                agntKPI5.Date__c = Date.today() - 6;
                agntKPI5.AU_DK_Handled_Count__c = 1; 

            insert agntKPI5;
            
            List<AggregateResult> agp = [SELECT SUM(AU_DK_Talk_Duration__c) myweeklytotal FROM Agent_KPI__c WHERE Program__c = :prog.Id AND Date__c = LAST_N_DAYS:7 AND Program_Contact__c =: progcon.Id];
            
            system.debug('agp' + agp);
            
            system.debug('agntKPI' + agntKPI.Program__c);
            system.debug('agntKPI' + agntKPI.Program_Contact__c);
            system.debug('agntKPI' + agntKPI.KPI_Record_ID__c);
            system.debug('agntKPI' + agntKPI.Date__c);
        
                 
                  System.runAs(newUser1) {
                  
                    WS_CommunitiesController ws_ComCon = new WS_CommunitiesController();
                    ws_ComCon.prog = prog;
                    ws_ComCon.getMyTotalCallsByProgram();
                    ws_ComCon.getMyTotalCallDurationsByProgram();
                    
                  }
                
            }
            
            static testMethod void TestgetMyTotalCallDurations() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Ttest';
                prog.Program_ID__c = 'TESTABC';
                prog.Client__c = acct.Id;
                
            insert prog;
            
            Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
            Account acc1 = new Account (
            Name = 'newAcc1'
            );  
            insert acc1;
            Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1'
            );
            insert conCase;
            //Create user
    
            Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
    
            User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
            //UserType = 'Guest'
            );
            insert newUser1;
            
            Program_Contact__c progcon = new Program_Contact__c();
                progcon.Contact__c = newUser1.contactId;
                progcon.Mapping_ID__c = 'TEST23';
            insert progcon;
                  
            Agent_KPI__c agntKPI = new Agent_KPI__c();
                agntKPI.Program__c = prog.Id;
                agntKPI.Program_Contact__c = progcon.Id;
                agntKPI.KPI_Record_ID__c = 'TEST123';
                agntKPI.Date__c = Date.today() - 4;
                agntKPI.AU_DK_Handled_Count__c = 1; 

            insert agntKPI;
    
            system.debug('@@0 ' + agntKPI.Id);
            system.debug('@@1 ' + agntKPI.Program_Contact__c);
            system.debug('@@2 ' + agntKPI.Date__c);
            
            
            List<AggregateResult> agp = [SELECT SUM(AU_DK_Talk_Duration__c) myweeklytotal FROM Agent_KPI__c WHERE Date__c = LAST_N_DAYS:7 AND Program_Contact__c =: progcon.Id];
            
            system.debug('agp' + agp);
            
            system.debug('agntKPI' + agntKPI.Program__c);
            system.debug('agntKPI' + agntKPI.Program_Contact__c);
            system.debug('agntKPI' + agntKPI.KPI_Record_ID__c);
            system.debug('agntKPI' + agntKPI.Date__c);
        
                 
                  System.runAs(newUser1) {
                  
                    WS_CommunitiesController ws_ComCon = new WS_CommunitiesController();
                    ws_ComCon.getMyTotalCallDurations();
                    ws_ComCon.getMyTotalCalls();
                    
                  }
                
            }
            
            
            static testMethod void forwardToCustomAuthPage() {
            
            Account acct = new Account();
                acct.Name = 'acctName';
            insert acct;
            
            Program__c prog = new Program__c();
                prog.Name = 'Atest';
                prog.Client__c = acct.Id;
                prog.Program_ID__c = 'ASD';
                
            insert prog;
            
            WS_CommunitiesController ws_dataCon = new WS_CommunitiesController();
            system.Test.setCurrentPageReference(new PageReference('Page.WS_CommunitiesPrograms')); 
            System.currentPageReference().getParameters().put('login',prog.Id);

            ws_dataCon.forwardToCustomAuthPage();

            
    }
      
}