<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Bounced__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Will show checked if the email bounced on delivery to the recipient</description>
        <externalId>false</externalId>
        <label>Bounced</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CampaignId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Related campaign for this email statistic</description>
        <externalId>false</externalId>
        <label>CampaignId</label>
        <referenceTo>Campaign</referenceTo>
        <relationshipLabel>Response Wise Emails</relationshipLabel>
        <relationshipName>Response_Wise_Emails</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Clicked__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Shows checked if a a recipient has clicked any links in the campaign email</description>
        <externalId>false</externalId>
        <label>Clicked</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ContactId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Related contact to which this email was sent</description>
        <externalId>false</externalId>
        <label>ContactId</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Response Wise Emails</relationshipLabel>
        <relationshipName>Response_Wise_Emails</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DSStatID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>DSStatID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LeadId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Related lead for this email</description>
        <externalId>false</externalId>
        <label>LeadId</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>Response Wise Emails</relationshipLabel>
        <relationshipName>Response_Wise_Emails</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opened__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Shows checked if the email recipient has opened the campaign email</description>
        <externalId>false</externalId>
        <label>Opened</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sent__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Shows checked once the email has been sent to the recipient</description>
        <externalId>false</externalId>
        <label>Sent</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Statistics__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>HYPERLINK(SUBSTITUTE(SUBSTITUTE(LEFT($Api.Partner_Server_URL_260, FIND(&apos;/services&apos;, $Api.Partner_Server_URL_260)), &quot;https://&quot;, &quot;https://responsewise.&quot;), &quot;.salesforce.&quot;, &quot;.visual.force.&quot;) &amp; &quot;apex/RWPage?height=700&amp;params=&quot; &amp;
&quot;Page%3DStats%26Action%3DNewsletters%26SubAction%3DViewSummary%26streamline%3D1%26id%3D&quot; &amp; TEXT(DSStatID__c),
&quot;Statistics for this Email&quot;
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Statistics</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unsubscribed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Shows checked if a user has unsubscribed from this email campaign</description>
        <externalId>false</externalId>
        <label>Unsubscribed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Response Wise Email</label>
    <nameField>
        <label>Response Wise Email Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Response Wise Emails</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
